package ru.ddndroider.sampleapp.data.gateway;

import java.util.List;

import io.reactivex.Single;
import ru.ddndroider.sampleapp.data.source.AtmDataSource;
import ru.ddndroider.sampleapp.domain.dto.Atm;
import ru.ddndroider.sampleapp.domain.gateway.AtmGateway;

public class AtmGatewayImpl implements AtmGateway {

    private final AtmDataSource dataSource;

    public AtmGatewayImpl(AtmDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Single<List<Atm>> getAtmList() {
        return Single.just(dataSource.getAtmList());
    }
}
