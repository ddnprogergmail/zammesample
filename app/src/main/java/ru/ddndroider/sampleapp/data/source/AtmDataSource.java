package ru.ddndroider.sampleapp.data.source;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.ddndroider.sampleapp.domain.dto.Atm;

public class AtmDataSource {

    @Inject
    public AtmDataSource() {
    }

    public List<Atm> getAtmList() {
        List<Atm> items = new ArrayList<>();
            items.add(new Atm(String.format("Название %d", 1), String.format("Описание %d", 1), 56.837723f, 60.604945f));
        items.add(new Atm(String.format("Название %d", 2), String.format("Описание %d", 2), 56.838336f, 60.590609f));
        items.add(new Atm(String.format("Название %d", 3), String.format("Описание %d", 3), 56.833875f, 60.591208f));
        items.add(new Atm(String.format("Название %d", 4), String.format("Описание %d", 4), 56.827015f, 60.610945f));
        items.add(new Atm(String.format("Название %d", 5), String.format("Описание %d", 5), 56.833818f, 60.629750f));
        items.add(new Atm(String.format("Название %d", 6), String.format("Описание %d", 6), 56.820907f, 60.537573f));
        items.add(new Atm(String.format("Название %d", 7), String.format("Описание %d", 7), 56.811000f, 60.615222f));
        items.add(new Atm(String.format("Название %d", 8), String.format("Описание %d", 8), 56.855663f, 60.602725f));
        return items;
    }


}
