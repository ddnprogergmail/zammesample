package ru.ddndroider.sampleapp.domain.interactor.atm;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import ru.ddndroider.sampleapp.domain.dto.Atm;
import ru.ddndroider.sampleapp.domain.dto._base.BaseUseCaseParams;
import ru.ddndroider.sampleapp.domain.gateway.AtmGateway;
import ru.ddndroider.sampleapp.domain.interactor._base.SchedulerProvider;
import ru.ddndroider.sampleapp.domain.interactor._base.SingleUseCase;

public class GetAtmUseCase extends SingleUseCase<List<Atm>, BaseUseCaseParams> {

    private final AtmGateway gateway;

    @Inject
    public GetAtmUseCase(SchedulerProvider schedulerProvider, AtmGateway gateway) {
        super(schedulerProvider);
        this.gateway = gateway;
    }

    @Override
    protected Single<List<Atm>> buildUseCaseSingle(BaseUseCaseParams params) {
        return gateway.getAtmList();
    }
}
