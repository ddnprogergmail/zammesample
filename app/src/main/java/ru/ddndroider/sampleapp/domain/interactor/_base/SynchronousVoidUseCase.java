package ru.ddndroider.sampleapp.domain.interactor._base;

import ru.ddndroider.sampleapp.domain.dto._base.BaseUseCaseParams;

public abstract class SynchronousVoidUseCase<T extends BaseUseCaseParams> extends BaseReactiveUseCase {

    public SynchronousVoidUseCase(SchedulerProvider schedulerProvider) {
        super(schedulerProvider);
    }

    public abstract void execute(T params);
}
