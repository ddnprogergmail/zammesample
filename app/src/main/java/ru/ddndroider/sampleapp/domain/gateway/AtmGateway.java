package ru.ddndroider.sampleapp.domain.gateway;

import java.util.List;

import io.reactivex.Single;
import ru.ddndroider.sampleapp.domain.dto.Atm;

public interface AtmGateway {

    Single<List<Atm>> getAtmList();
}
