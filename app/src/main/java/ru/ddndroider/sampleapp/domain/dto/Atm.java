package ru.ddndroider.sampleapp.domain.dto;

import java.util.UUID;

public class Atm {

    private String id;
    private String name;
    private String description;

    private float lat;
    private float lng;

    public Atm(String name, String description, float lat, float lng) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.description = description;
        this.lat = lat;
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }
}
