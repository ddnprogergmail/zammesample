package ru.ddndroider.sampleapp.domain.interactor._base;

import ru.ddndroider.sampleapp.domain.dto._base.BaseUseCaseParams;

public abstract class SynchronousUseCase<R, T extends BaseUseCaseParams> extends BaseReactiveUseCase {

    public SynchronousUseCase(SchedulerProvider schedulerProvider) {
        super(schedulerProvider);
    }

    public abstract R execute(T params);

    public R execute() {
        return execute(null);
    }
}
