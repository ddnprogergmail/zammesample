package ru.ddndroider.sampleapp.app.ui._base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.ClusterRenderer;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import ru.ddndroider.sampleapp.R;
import ru.ddndroider.sampleapp.app.ui.map.AtmGMapsInfoAdapter;
import ru.ddndroider.sampleapp.domain.dto.Atm;

public class MapLayout extends FrameLayout {

    private CompositeDisposable disposable = new CompositeDisposable();
    private ClusterManager<AtmClusterItem> clusterManager;
    private Map<Cluster<AtmClusterItem>, Marker> clusterMarkerMap = new HashMap<>();

    private Subject<GoogleMap> mapSubject;

    public MapLayout(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public MapLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MapLayout(@NonNull Context context, @Nullable AttributeSet attrs,
                     @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();

        if (!isInEditMode()) {
            FragmentTransaction fragmentTransaction =
                    ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(getId(), mapFragment);
            fragmentTransaction.commit();

            mapSubject = BehaviorSubject.create();
            Observable.create(
                    (ObservableOnSubscribe<GoogleMap>) e -> mapFragment.getMapAsync(googleMap -> {
                        googleMap.getUiSettings().setZoomControlsEnabled(false);
                        clusterManager = new ClusterManager<>(getContext(), googleMap);
                        clusterManager.setRenderer(new AtmClusterRenderer(getContext(), googleMap, clusterManager));
                        googleMap.setOnCameraIdleListener(clusterManager);
                        googleMap.setOnMarkerClickListener(clusterManager);
                        googleMap.setInfoWindowAdapter(new AtmGMapsInfoAdapter(getContext()));
                        clusterManager.setOnClusterClickListener(cluster -> {
                            String firstName = cluster.getItems().iterator().next().getTitle();
                            Toast.makeText(getContext(), cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();

                            LatLngBounds.Builder builder = LatLngBounds.builder();
                            for (ClusterItem item : cluster.getItems()) {
                                builder.include(item.getPosition());
                            }
                            final LatLngBounds bounds = builder.build();
                            try {
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, (int) getResources().getDimension(R.dimen.very_large_margin)));
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }

                            return true;
                        });

                        e.onNext(googleMap);
                    }))
                    .subscribe(mapSubject);
        }
    }

    public void addMarkers(List<Atm> atmList) {
        disposable.add(mapSubject
                .observeOn(AndroidSchedulers.mainThread()).subscribe(googleMap -> {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();

                    for (Atm atm : atmList) {
                        clusterManager.addItem(new AtmClusterItem(atm));
                        builder.include(new LatLng(atm.getLat(), atm.getLng()));
                    }
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), (int) getResources().getDimension(R.dimen.very_large_margin)));
                    clusterManager.cluster();
                }));
    }

    public void setOnInfoWindowClickListener(GoogleMap.OnInfoWindowClickListener onInfoWindowClickListener) {
        disposable.add(mapSubject.subscribe(googleMap -> {
            googleMap.setOnInfoWindowClickListener(onInfoWindowClickListener);
        }));
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public void search(String searchString) {
        disposable.add(mapSubject
                .observeOn(AndroidSchedulers.mainThread()).subscribe(googleMap -> {
                    for (AtmClusterItem atmClusterItem : clusterManager.getAlgorithm().getItems()) {
                        Atm atm = atmClusterItem.getAtm();
                        if (atm != null) {
                            if (atm.getName().toLowerCase().contains(searchString) || atm.getDescription().toLowerCase().contains(searchString)) {
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(atm.getLat(), atm.getLng()), 17));
                                clusterManager.cluster();
                                break;
                            }
                        }
                    }
                }));
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int count = canvas.save();

        final Path path = new Path();

        float[] cornerDimensions = {
                getResources().getDimension(R.dimen.standard_margin), getResources().getDimension(R.dimen.standard_margin),
                getResources().getDimension(R.dimen.standard_margin), getResources().getDimension(R.dimen.standard_margin),
                0, 0,
                0, 0};

        path.addRoundRect(new RectF(0, 0, canvas.getWidth(), canvas.getHeight())
                , cornerDimensions, Path.Direction.CW);

        canvas.clipPath(path);

        super.dispatchDraw(canvas);
        canvas.restoreToCount(count);
    }

    private class AtmClusterItem implements ClusterItem {

        private Atm atm;

        public AtmClusterItem(Atm atm) {
            this.atm = atm;
        }

        public Atm getAtm() {
            return atm;
        }

        @Override
        public LatLng getPosition() {
            return new LatLng(atm.getLat(), atm.getLng());
        }

        @Override
        public String getTitle() {
            return atm.getName();
        }

        @Override
        public String getSnippet() {
            return atm.getDescription();
        }

    }

    private class AtmClusterRenderer extends DefaultClusterRenderer<AtmClusterItem> {

        public AtmClusterRenderer(Context context, GoogleMap map, ClusterManager<AtmClusterItem> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onClusterItemRendered(AtmClusterItem clusterItem, Marker marker) {
            marker.setTag(clusterItem.getAtm());
            super.onClusterItemRendered(clusterItem, marker);
        }

        @Override
        protected void onBeforeClusterItemRendered(AtmClusterItem item, MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<AtmClusterItem> cluster) {
            return cluster.getSize() > 2;
        }

        @Override
        protected void onClusterRendered(Cluster<AtmClusterItem> cluster, Marker marker) {
            super.onClusterRendered(cluster, marker);
            clusterMarkerMap.put(cluster, marker);
        }
    }
}
