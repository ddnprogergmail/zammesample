package ru.ddndroider.sampleapp.app.navigation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import ru.ddndroider.sampleapp.app.ui._base.transitions.ForwardEnterTransition;
import ru.ddndroider.sampleapp.app.ui._base.transitions.ForwardExitTransition;
import ru.ddndroider.sampleapp.app.ui._base.transitions.ForwardReenterTransition;
import ru.ddndroider.sampleapp.app.ui._base.transitions.ForwardReturnTransition;
import ru.ddndroider.sampleapp.app.ui._base.transitions.ReplaceEnterTransition;
import ru.ddndroider.sampleapp.app.ui._base.transitions.ReplaceExitTransition;
import ru.ddndroider.sampleapp.app.ui._base.transitions.ReplaceReenterTransition;
import ru.ddndroider.sampleapp.app.ui._base.transitions.ReplaceReturnTransition;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;
import ru.terrakok.cicerone.commands.Replace;

public class ApplicationNavigator extends SupportAppNavigator {

    public ApplicationNavigator(FragmentActivity activity, FragmentManager fragmentManager, int containerId) {
        super(activity, fragmentManager, containerId);
    }

    @Override
    protected void setupFragmentTransaction(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
        if (command instanceof Replace) {
            initReplaceAnimation(currentFragment, nextFragment);
        } else if (command instanceof Forward) {
            initForwardAnimation(currentFragment, nextFragment);
        }
    }

    private void initReplaceAnimation(Fragment currentFragment, Fragment nextFragment) {
        if (currentFragment != null) {
            currentFragment.setExitTransition(new ReplaceExitTransition());
            currentFragment.setReenterTransition(new ReplaceReenterTransition());
        }
        nextFragment.setEnterTransition(new ReplaceEnterTransition());
        nextFragment.setReturnTransition(new ReplaceReturnTransition());
    }

    private void initForwardAnimation(Fragment currentFragment, Fragment nextFragment) {
        if (currentFragment != null) {
            currentFragment.setExitTransition(new ForwardExitTransition());
            currentFragment.setReenterTransition(new ForwardReenterTransition());
        }
        nextFragment.setEnterTransition(new ForwardEnterTransition());
        nextFragment.setReturnTransition(new ForwardReturnTransition());
    }
}
