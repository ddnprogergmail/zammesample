package ru.ddndroider.sampleapp.app.presentation.map;

import java.util.List;

import ru.ddndroider.sampleapp.app.presentation._base.BaseView;
import ru.ddndroider.sampleapp.domain.dto.Atm;

public interface MapView extends BaseView {

    void showAtmList(List<Atm> atmList);
}
