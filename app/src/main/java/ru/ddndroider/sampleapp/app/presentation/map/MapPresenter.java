package ru.ddndroider.sampleapp.app.presentation.map;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;
import ru.ddndroider.sampleapp.app.presentation._base.BasePresenter;
import ru.ddndroider.sampleapp.app.presentation._base.ResourceManager;
import ru.ddndroider.sampleapp.domain.dto.Atm;
import ru.ddndroider.sampleapp.domain.interactor.atm.GetAtmUseCase;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class MapPresenter extends BasePresenter<MapView> {

    private final GetAtmUseCase getAtmUseCase;

    public MapPresenter(Router router, ResourceManager resourceManager, GetAtmUseCase getAtmUseCase) {
        super(router, resourceManager);
        this.getAtmUseCase = getAtmUseCase;

        addUseCase(this.getAtmUseCase);
    }

    @Override
    protected void onFirstViewAttach() {
        getAtmUseCase.execute(new DisposableSingleObserver<List<Atm>>() {
            @Override
            public void onSuccess(List<Atm> atmList) {
                getViewState().showAtmList(atmList);
            }

            @Override
            public void onError(Throwable e) {
                showError(e);
            }
        });
    }

    public void onShowDetail(Atm atm) {

    }
}
