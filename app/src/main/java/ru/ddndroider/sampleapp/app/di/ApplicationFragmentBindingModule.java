package ru.ddndroider.sampleapp.app.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.ddndroider.sampleapp.app.ui.map.MapFragment;

@Module
public abstract class ApplicationFragmentBindingModule {

    @ContributesAndroidInjector
    abstract MapFragment mapFragment();
}
