package ru.ddndroider.sampleapp.app.navigation;

import android.support.v4.app.Fragment;

import ru.ddndroider.sampleapp.app.ui.map.MapFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class MapScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return new MapFragment();
        }
    }
}
