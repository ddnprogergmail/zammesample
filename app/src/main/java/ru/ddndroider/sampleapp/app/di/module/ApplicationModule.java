package ru.ddndroider.sampleapp.app.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.ddndroider.sampleapp.data.gateway.AtmGatewayImpl;
import ru.ddndroider.sampleapp.data.source.AtmDataSource;
import ru.ddndroider.sampleapp.domain.gateway.AtmGateway;
import ru.ddndroider.sampleapp.domain.interactor._base.SchedulerProvider;

@Module(includes = {ContextModule.class, NavigationModule.class, LocalNavigationModule.class})
public class ApplicationModule {

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider() {
            @Override
            public Scheduler io() {
                return Schedulers.io();
            }

            @Override
            public Scheduler ui() {
                return AndroidSchedulers.mainThread();
            }

            @Override
            public Scheduler computation() {
                return Schedulers.computation();
            }
        };
    }

    @Provides
    @Singleton
    AtmGateway provideAtmGateway(AtmDataSource dataSource) {
        return new AtmGatewayImpl(dataSource);
    }
}
