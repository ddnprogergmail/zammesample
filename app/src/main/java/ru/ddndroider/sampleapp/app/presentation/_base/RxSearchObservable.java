package ru.ddndroider.sampleapp.app.presentation._base;

import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

public class RxSearchObservable {

    public static Flowable<String> fromView(AppCompatEditText searchView) {
        return Flowable.create(e ->
                searchView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                            e.onNext(s.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                }), BackpressureStrategy.LATEST);
    }
}
