package ru.ddndroider.sampleapp.app.navigation;

public interface BackButtonListener {

    boolean onBackPressed();
}
