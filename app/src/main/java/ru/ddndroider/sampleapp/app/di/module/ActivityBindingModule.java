package ru.ddndroider.sampleapp.app.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.ddndroider.sampleapp.app.di.ApplicationFragmentBindingModule;
import ru.ddndroider.sampleapp.app.ui.ApplicationActivity;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {ApplicationFragmentBindingModule.class})
    abstract ApplicationActivity bindApplication();
}
