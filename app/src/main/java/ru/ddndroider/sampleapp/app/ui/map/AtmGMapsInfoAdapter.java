package ru.ddndroider.sampleapp.app.ui.map;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import ru.ddndroider.sampleapp.R;
import ru.ddndroider.sampleapp.app.ui._base.Layout;

public class AtmGMapsInfoAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public AtmGMapsInfoAdapter(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_gmaps_info_adapter, null, false);

        AppCompatTextView textTitle = view.findViewById(R.id.textTitle);
        AppCompatTextView textDescription = view.findViewById(R.id.textDescription);

        textTitle.setText(marker.getTitle());
        textDescription.setText(marker.getSnippet());

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
