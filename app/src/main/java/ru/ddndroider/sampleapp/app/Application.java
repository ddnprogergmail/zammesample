package ru.ddndroider.sampleapp.app;

import android.content.Context;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import ru.ddndroider.sampleapp.app.di.ApplicationComponent;
import ru.ddndroider.sampleapp.app.di.DaggerApplicationComponent;

public class Application extends DaggerApplication {

    private static Context sContext;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);

        return component;
    }
}