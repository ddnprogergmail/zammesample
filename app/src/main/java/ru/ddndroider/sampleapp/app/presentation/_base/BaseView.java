package ru.ddndroider.sampleapp.app.presentation._base;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by ddn on 25.03.2018.
 */

public interface BaseView extends MvpView {

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showMessage(String message);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showSuccessToast(String message);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showErrorToast(String message);
}
