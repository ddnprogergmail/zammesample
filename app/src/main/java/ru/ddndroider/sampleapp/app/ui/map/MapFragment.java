package ru.ddndroider.sampleapp.app.ui.map;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.ddndroider.sampleapp.R;
import ru.ddndroider.sampleapp.app.navigation.BackButtonListener;
import ru.ddndroider.sampleapp.app.presentation._base.ResourceManager;
import ru.ddndroider.sampleapp.app.presentation._base.RxSearchObservable;
import ru.ddndroider.sampleapp.app.presentation.map.MapPresenter;
import ru.ddndroider.sampleapp.app.presentation.map.MapView;
import ru.ddndroider.sampleapp.app.ui._base.BaseFragment;
import ru.ddndroider.sampleapp.app.ui._base.Layout;
import ru.ddndroider.sampleapp.app.ui._base.MapLayout;
import ru.ddndroider.sampleapp.domain.dto.Atm;
import ru.ddndroider.sampleapp.domain.interactor.atm.GetAtmUseCase;
import ru.terrakok.cicerone.Router;

@Layout(id = R.layout.fragment_map)
public class MapFragment extends BaseFragment implements MapView, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, BackButtonListener{

    @BindView(R.id.textTitle)
    AppCompatTextView textTitle;
    @BindView(R.id.textDescription)
    AppCompatTextView textDescription;

    @BindView(R.id.bottomSheetLayout)
    View bottomSheetLayout;
    @BindView(R.id.editSearch)
    AppCompatEditText editSearch;

    @Inject
    Router router;
    @Inject
    ResourceManager resourceManager;
    @Inject
    GetAtmUseCase getAtmUseCase;

    @BindView(R.id.mapLayout)
    MapLayout mapLayout;

    private Disposable searchDisposable;
    private String lastSearch;

    private BottomSheetBehavior bottomSheetBehavior;

    @InjectPresenter
    MapPresenter presenter;

    @ProvidePresenter
    MapPresenter providePresenter() {
        return new MapPresenter(router, resourceManager, getAtmUseCase);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapLayout.setOnInfoWindowClickListener(this);
        searchDisposable = RxSearchObservable.fromView(editSearch)
                .debounce(250, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .filter(s -> !s.isEmpty())
                .filter(s -> !s.equalsIgnoreCase(lastSearch))
                .doOnNext(s -> {
                    lastSearch = s;
                    mapLayout.search(s);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                }, Throwable::printStackTrace);

        editSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (i == BottomSheetBehavior.STATE_EXPANDED) {
                    getView().requestFocus();
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        hideKeyboard();
        Atm atm = (Atm) marker.getTag();
        textTitle.setText(atm.getName());
        textDescription.setText(atm.getDescription());
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onDestroyView() {
        if (searchDisposable != null && !searchDisposable.isDisposed()) {
            searchDisposable.dispose();
        }
        super.onDestroyView();
    }

    @Override
    public void showAtmList(List<Atm> atmList) {
        mapLayout.addMarkers(atmList);
    }

    @Override
    public boolean onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return true;
        }
        if (!editSearch.getText().toString().isEmpty()) {
            editSearch.setText(null);
            hideKeyboard();
            return true;
        }

        return false;
    }
}
