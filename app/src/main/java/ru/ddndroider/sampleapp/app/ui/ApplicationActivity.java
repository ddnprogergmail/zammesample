package ru.ddndroider.sampleapp.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import javax.inject.Inject;

import ru.ddndroider.sampleapp.R;
import ru.ddndroider.sampleapp.app.navigation.ApplicationNavigator;
import ru.ddndroider.sampleapp.app.navigation.BackButtonListener;
import ru.ddndroider.sampleapp.app.navigation.RouterProvider;
import ru.ddndroider.sampleapp.app.navigation.Screens;
import ru.ddndroider.sampleapp.app.presentation._base.ResourceManager;
import ru.ddndroider.sampleapp.app.ui._base.BaseActivity;
import ru.ddndroider.sampleapp.app.ui._base.Layout;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

@Layout(id = R.layout.activity_application)
public class ApplicationActivity extends BaseActivity implements RouterProvider {

    @Inject
    NavigatorHolder holder;
    @Inject
    Router router;
    @Inject
    ResourceManager resourceManager;

    private Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        navigator = new ApplicationNavigator(this, getSupportFragmentManager(), R.id.fragmentContainer);
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            router.newRootScreen(new Screens.MapScreen());
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        holder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        holder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(fragment instanceof BackButtonListener) || !((BackButtonListener) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public Router getRouter() {
        return router;
    }
}
