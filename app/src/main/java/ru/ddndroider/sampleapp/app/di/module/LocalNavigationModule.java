package ru.ddndroider.sampleapp.app.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.ddndroider.sampleapp.app.navigation.LocalCiceroneHolder;

@Module
public class LocalNavigationModule {

    @Provides
    @Singleton
    LocalCiceroneHolder provideLocalNavigationHolder() {
        return new LocalCiceroneHolder();
    }
}
