package ru.ddndroider.sampleapp.app.presentation._base;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.StringRes;

import javax.inject.Inject;

public class ResourceManager {

    private final Context context;

    @Inject
    public ResourceManager(Context context) {
        this.context = context;
    }

    public String getString(@StringRes int stringRes) {
        return context.getResources().getString(stringRes);
    }

    public String getString(@StringRes int stringRes, Object... objects) {
        return context.getResources().getString(stringRes, objects);
    }

    public int getColor(@ColorRes int colorRes) {
        return context.getResources().getColor(colorRes);
    }

    public float getDimen(@DimenRes int dimenRes) {
        return context.getResources().getDimension(dimenRes);
    }
}
