package ru.ddndroider.sampleapp.app.navigation;

import ru.terrakok.cicerone.Router;

public interface RouterProvider {

    Router getRouter();
}
